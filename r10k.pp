#
#R10k configuration manifest
#
#requires puppetlabs r10k module
# /opt/puppetlab/bin/puppet module install puppet-r10k
#
#fact can be set from env variable FACTER_giturl
$git_url=$facts['giturl']
class { 'r10k':
  remote => $git_url,
}

