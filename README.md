To setup R10k run
  sudo FACTER_giturl=<git-repository-url> puppet apply r10k.pp
  
To update hiera config to user version 5 run
  sudo puppet apply hiera5.pp